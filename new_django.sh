while [[ true ]]
do
	echo "Input a project name: "
	read project_name
	echo "The project name is \"$project_name\". Confirm? (y/n)"
	read answer
	if [[ answer -eq "y" ]]; then break; fi
done

mkdir $project_name

cd ./$project_name

# Create venv
echo "Create venv"
python -m venv venv
echo "Create venv is ended"

# Activate venv
# linux
# source ./venv/bin/activate

# windows
source ./venv/Scripts/activate

# Install Django-framework
pip install django

# Create Django project
django-admin startproject "$project_name"

# Deactivate venv
deactivate

# Remove venv in project dir
# mv ./venv ./$project_name

