#!//bin/bash
if [[ $# -ne 1 ]]; then echo 'Необходиомо указать путь к файлу реестра'; exit; fi
config_file="$HOME/.ssh/config"
IFS=$'\n'
for i in $(cat -s $1)
do
    path=$(echo $i | grep -P -o "(ansible_private_key_file)\S+" | cut -f 2 -d '=' )
    host=$(echo $i | grep -P -o "(ansible_host)\S+" | cut -f 2 -d '=' )
    user=$(echo $i | grep -P -o "(ansible_user)\S+" | cut -f 2 -d '=' )

    if [[ $path = "" ]]; then continue; fi
    if [ -f "$HOME/.ssh/$(basename $path)" ]
    then
        echo "ключ $path существует"
    else
        echo "создается новый ключ по пути $path"
        ssh-keygen -q -t rsa -N "" -f $HOME/.ssh/$(basename $path)
        echo "" >> $config_file
        echo "Host $host" >> $config_file
        echo "    HostName $host" >> $config_file
        echo "    User $user" >> $config_file
        echo "    IdentityFile $path" >> $config_file
        echo "    TCPKeepAlive yes" >> $config_file
        echo "    IdentitiesOnly yes"  >> $config_file
    fi
done

exit
