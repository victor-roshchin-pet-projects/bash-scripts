#!/usr/bin/bash

soft=$(dpkg-query -l | grep imagemagick)

echo 'Проверка установки программы imagemagick'
if [[ $soft -eq "" ]]; then echo "программа не установлена, производится установка ПО"; fi

function formating {
    f=$( file -b $1 | grep -P -o "\d+[xX]{1}\d+" ); w=$(echo $f | cut -f 1 -d 'x' ); h=$(echo $f | cut -f 2 -d 'x' ) ; mime=$(file --mime-type -b $1)
    if [[ $mime = "image/jpeg" ]]
    then
        new_image="$(echo $1 | cut -f 1 -d '.' )_thumbnai.jpg"
        if [[ $w -ge $h ]]
        then
            echo "Сжатие изображения $1 по ширине в программе и сохранении сжатого изображения $new_image"
            cp $1 $new_image
        else
            echo "Сжатие изображения $1 по высоте в программе и сохранении сжатого изображения $new_image"
            cp $1 $new_image
        fi
    fi
}

function formatImage {
    IFS=$'\n'
    if [[ $# -eq 1 ]]
    then
        for i in $(cat $1 | grep -i .jpg | grep -v _thumbnai)
        do
            formating $i
        done
    else
        for i in $(ls -a $1 | grep -i .jpg | grep -v _thumbnai)
        do
            formating "$1/$i"
        done
    fi

}

if [[ $# -eq 1 ]]
then
    formatImage $1
else
    scriptdir="$(pwd)$(dirname $0 | cut -f 2 -d '.')/"
    echo $scriptdir
    formatImage $scriptdir 1
fi

exit
